### Forecast Outcomes for Patients in a Cardiac Unit

Pseudo data contains information about patients in a cardiac unit (e.g.base heart rate and echocardiagram results) the model built and trained on this data predicts negative/positive outcomes based on information we have about the patient.

### File Descriptions and Read/Write Order
* pat_outcomes.csv: raw data 
* pat_outcomes_clean.csv: clean and imputed data 
* pat_outcomes_README.txt: description of data
* PredictPatientOutcomes.R: script to run model  
* train.csv: training data
* test.csv: testing validation data
* README.md

pat_outcomes.csv -> PredictPatientOutcomes.R -> pat_outcomes_clean.csv -> PredictPatientOutcomes.R-> train.csv,test.csv, PredictPatientOutcomes.R

### R version
R version 3.4.2 (2017-09-28)
"Short Summer"

## Author

* **Elizabeth Johnson** 

